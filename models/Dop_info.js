const mongoose = require('mongoose');

const InfoSchema = new mongoose.Schema({
  _id: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  phone: {
    type: String,
    required: true
  },
  city: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

const Info = mongoose.model('Info', InfoSchema);

module.exports = Info;
